#!/bin/bash
# Prevents screensaver (and, obviously, hibernation)	when videos/slideshows/Skype/...
# Prevents hibernation (but still allows screensaver)	when music/torrents/...
# Dependencies:						XFCE, bc, imagemagick, pulseaudio, scrot, xdotool, xprintidle, xssstate

# ======= Variables =======
# Programmes that prevent hibernation (screen can turn off)
WHITELIST=("transmission-gtk" )

# Minimum tolerable integrity (rate in %) - value too high will make clock prevent screensaver
PRECISION=91.5

# Screensaver delay (in minutes)
DELAY=1

# Delay for hibernation
HIBERNATE_AFTER=20

HIBERNATE_AC_AFTER=$(xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/inactivity-on-ac)

HIBERNATE_BATTERY_AFTER=$(xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/inactivity-on-battery)

if [ "$HIBERNATE_AC_AFTER" > 0 ]; then
	HIBERNATE_AFTER=$HIBERNATE_AC_AFTER
else
	if [ "$HIBERNATE_BATTERY_AFTER" > 0 ]; then
		HIBERNATE_AFTER=$HIBERNATE_BATTERY_AFTER
	fi
fi

echo "Hibernation delay (min): $HIBERNATE_AFTER"

# ======= Programme =======
INTEGRITY=100				# screenshot similarity to previous
ITER=true				# iteration (to call picture)

scrot -z ~/.true.jpg
scrot -z ~/.false.jpg

DELAY_SCREENSHOT=$(echo "$DELAY*57" | bc -l) # delay in seconds
DELAY_MS=$(echo "$DELAY*50000" | bc -l) # delay in milliseconds

echo "Integrity: $INTEGRITY. Screenshot delay: $DELAY_SCREENSHOT "

while [ true ]; do
	IDLE="$(xprintidle)"
	# When screen is on
	if [ "$(xssstate -s)" = "off" ] || [ "$(xssstate -s)" = "disabled" ]; then
		if (( $(echo "$INTEGRITY < $PRECISION" | bc -l) )) && (( $(echo "$DELAY_MS > $IDLE" | bc -l) )); then # Reset timer of idleness if change is significant
			xdotool key F13
			echo "Screensaver prevented"
		fi
		INTEGRITY=$(echo "$(compare -metric ncc ~/.true.jpg ~/.false.jpg null: 2>&1)*100.0" | bc -l)
		scrot -z -d 10 ~/."$ITER".jpg # Screenshot without GUI
		if [ "$ITER" = "true" ]; then
			ITER=false
		else
			ITER=true
		fi
	# Screen is off
	else
		if (( $(echo "$DELAY_MS > $IDLE" | bc -l) )); then # Reset timer of idleness on user input
			xdotool key F13
			echo "User input detected"
		else
			PREVENT=false
			if (( $(echo "$(pacmd list-sink-inputs | grep -c 'state: RUNNING') > 0" | bc -l) )); then
				# Prevent hibernation if playing audio is detected
				PREVENT=true
				echo "Audio is playing"
			else
				for i in "${WHITELIST[@]}"; do
					if [ ! -z "$(pidof $i)" ]; then
						# Prevent hibernation if whitelisted app is detected
						PREVENT=true
						echo "Whitelisted programme is running"
						break
					fi
				done
			fi
			if [ "$PREVENT" = "true" ]; then
				xdotool key F13
				sleep 2
				xset dpms force off
				echo "Hibernation prevented"
				INTEGRITY=0
			else
				echo "Hibernation upcoming"
			fi
		fi
	fi
done
